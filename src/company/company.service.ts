import { HttpException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ICompany } from './interfaces/company.interface';
import { CreateCompanyDto, UpdateCompanyDto } from './company.dto';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class CompanyService {
  constructor(
    @InjectModel('Company') private readonly companyModel: Model<ICompany>,
  ) {}

  public async getCompany() {
    const company = await this.companyModel.find().exec();
    if (!company || !company[0]) {
      throw new HttpException('Company Not Found', 404);
    }
    return company;
  }

  public async postCompany(newCompany: CreateCompanyDto) {
    const company = await this.companyModel.create(newCompany);
    return company;
  }

  public async getCompanyById(id: number) {
    const company = await this.companyModel.findOne({ id }).exec();
    if (!company) {
      throw new HttpException('Company Not Found', 404);
    }
    return company;
  }

  public async deleteCompanyById(id: number) {
    const company = await this.companyModel.deleteOne({ id }).exec();
    if (company.deletedCount === 0) {
      throw new HttpException('Company Not Found', 404);
    }
    return company;
  }

  public async putCompanyById(id: number, company: UpdateCompanyDto) {
    const updatedCompany = await this.companyModel.findOneAndUpdate(
      { id },
      company,
      {
        new: true,
      },
    );
    if (!updatedCompany) {
      throw new HttpException('Company Not Found', 404);
    }
    return updatedCompany;
  }
}
