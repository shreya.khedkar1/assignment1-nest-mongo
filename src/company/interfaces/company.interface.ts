/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface ICompany extends Document {
  readonly id: number;
  readonly companyName: string;
}
