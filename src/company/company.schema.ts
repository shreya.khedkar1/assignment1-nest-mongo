/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const CompanySchema = new mongoose.Schema({
  companyName: String,
});
