import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateCompanyDto, UpdateCompanyDto } from './company.dto';
import { CompanyService } from './company.service';

@Controller('company')
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  @Get()
  async getCompany() {
    const result = await this.companyService.getCompany();
    return result;
  }

  @Post()
  async postCompany(@Body() company: CreateCompanyDto) {
    const result = await this.companyService.postCompany(company);
    return result;
  }

  @Get(':id')
  async getCompanyById(@Param('id') id: number) {
    const result = await this.companyService.getCompanyById(id);
    return result;
  }

  @Delete(':id')
  async deleteCompanyById(@Param('id') id: number) {
    const result = await this.companyService.deleteCompanyById(id);
    return result;
  }

  @Put(':id')
  async putCompanyById(
    @Param('id') id: number,
    @Body() company: UpdateCompanyDto,
  ) {
    const result = await this.companyService.putCompanyById(id, company);
    return result;
  }
}
