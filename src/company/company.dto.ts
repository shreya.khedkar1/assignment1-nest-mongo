/* eslint-disable prettier/prettier */
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateCompanyDto {
  @IsNotEmpty()
  @IsString()
  readonly companyName: string;
}

export class UpdateCompanyDto {
  @IsNotEmpty()
  @IsString()
  readonly companyName: string;
}
