import { Injectable, HttpException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { IProduct } from './interfaces/product.interface';
import { ProductDto, updateProductDto } from './product.dto';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel('Product') private readonly productModel: Model<IProduct>,
  ) {}

  public async getProducts() {
    const products = await this.productModel.find().exec();
    if (!products || !products[0]) {
      throw new HttpException('Product Not Found', 404);
    }
    return products;
  }

  public async postProduct(newProduct: ProductDto) {
    const product = await this.productModel.create(newProduct);
    return product;
  }

  public async getProductById(id: number) {
    const products = await this.productModel.findOne({ id }).exec();
    if (!products) {
      throw new HttpException('Product Not Found', 404);
    }
    return products;
  }

  public async getProductByCompanyId(companyId: string) {
    const products = await this.productModel.find({ companyId }).exec();
    if (!products) {
      throw new HttpException('No products of this company', 404);
    }
    return products;
  }

  public async deleteProductById(id: number) {
    const products = await this.productModel.deleteOne({ id }).exec();
    if (products.deletedCount === 0) {
      throw new HttpException('Product Not Found', 404);
    }
    return products;
  }

  public async putProductById(id: number, product: updateProductDto) {
    const updatedProduct = await this.productModel.findOneAndUpdate(
      { id },
      product,
      {
        new: true,
      },
    );
    if (!updatedProduct) {
      throw new HttpException('Product Not Found', 404);
    }
    return updatedProduct;
  }
}
