/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({
  id: Number,
  companyId: {
    type: mongoose.Types.ObjectId,
    ref: 'Company',
  },
  title: String,
  price: Number,
  description: String,
  category: String,
  image: String,
  rating: Object,
});
