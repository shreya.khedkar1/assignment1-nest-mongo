/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface IProduct extends Document {
  readonly id: number;
  readonly title: string;
  readonly price: number;
  readonly description: string;
  readonly category: string;
  readonly image: string;
  readonly rating: object;
}
