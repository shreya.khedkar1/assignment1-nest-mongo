import {
  Controller,
  Post,
  Delete,
  Get,
  Put,
  Body,
  Param,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductDto, updateProductDto } from './product.dto';

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Get()
  async getProducts() {
    const result = await this.productService.getProducts();
    return result;
  }

  @Post()
  async postProduct(@Body() product: ProductDto) {
    const result = await this.productService.postProduct(product);
    return result;
  }

  @Get(':id')
  async getProductById(@Param('id') id: number) {
    const result = await this.productService.getProductById(id);
    return result;
  }

  @Get('companyId/:companyId')
  async getProductByCompanyId(@Param('companyId') companyId: string) {
    const result = await this.productService.getProductByCompanyId(companyId);
    return result;
  }

  @Delete(':id')
  async deleteProductById(@Param('id') id: number) {
    const result = await this.productService.deleteProductById(id);
    return result;
  }

  @Put(':id')
  async putProductById(
    @Param('id') id: number,
    @Body() product: updateProductDto,
  ) {
    const result = await this.productService.putProductById(id, product);
    return result;
  }
}
