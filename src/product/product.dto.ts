/* eslint-disable prettier/prettier */
import { IsString, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class ProductDto {
  @IsNotEmpty()
  @IsNumber()
  readonly id: number;

  @IsNotEmpty()
  readonly companyId: string;

  @IsNotEmpty()
  @IsString()
  readonly title: string;

  @IsNotEmpty()
  @IsNumber()
  readonly price: number;

  @IsString()
  readonly description: string;

  @IsString()
  readonly category: string;
  readonly image: string;
  readonly rating: object;
}

export class updateProductDto {
  @IsString()
  @IsOptional()
  readonly companyId: string;

  @IsString()
  @IsOptional()
  readonly title?: string;

  @IsNumber()
  @IsOptional()
  readonly price?: number;

  @IsString()
  @IsOptional()
  readonly description?: string;

  @IsString()
  @IsOptional()
  readonly category?: string;

  @IsOptional()
  readonly image?: string;

  @IsOptional()
  readonly rating?: object;
}
